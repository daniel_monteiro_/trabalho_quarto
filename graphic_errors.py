# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import datetime


def generate_graphic_pdf(list_errors):
    plt.plot(list_errors)
    plt.ylabel("Porcentagem média de erro")
    plt.xlabel("Rodada de treinamento")

    plt.savefig('graphics/error_' + datetime.datetime.now().strftime("%d_%m_%Y__%H_%M_%S") + '.pdf')