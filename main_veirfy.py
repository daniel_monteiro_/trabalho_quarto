import csv
import random
from graphic_errors import *

arquivo = open('classificacao_Q3.csv', 'r')
leitor = csv.reader(arquivo)

dic_genero = {'F': 0, 'M': 1}
dic_idade = {'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4}
dic_escolaridade = {'Fundamental': 0, 'Medio': 1, 'Superior': 2, 'Pos-graduacao': 3}
dic_profissao = {'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4}
dic_alvo = {'0': 0, '1': 1}

# Lista com todos os valores do arquivo de classificacao_Q3
lista = list(leitor)

for linha in lista[1:]:
    # Transformacao Genero
    linha[0] = dic_genero[linha[0]]
    # Transformacao Idade
    linha[1] = linha[1][:1]
    linha[1] = dic_idade[linha[1]]
    # Tranformacao Escolaridade
    linha[2] = dic_escolaridade[linha[2]]
    # Transformacao Profissao
    linha[3] = dic_profissao[linha[3]]
    # Transformacao Profissao
    linha[4] = dic_alvo[linha[4]]


def filtrarLista(lista, valor, i):
    filtrada = list(filter(lambda x: x[i] == valor, lista))
    return filtrada


def calcularProbabilidadeBayes(x, listaCompleta):
    listaAlvo0 = filtrarLista(listaCompleta, 0, 4)
    listaAlvo1 = filtrarLista(listaCompleta, 1, 4)
    probabilidadesA0 = list()
    probabilidadesA1 = list()

    resultado0 = 1
    resultado1 = 1

    # 
    for index in range(len(x) - 1):
        qtdOcorrencias0Dadox = len(filtrarLista(listaAlvo0, x[index], index))
        qtdOcorrencias1Dadox = len(filtrarLista(listaAlvo1, x[index], index))

        ## CORRECAO DE LAPLACE
        # if (qtdOcorrencias0Dadox == 0):
        #    qtdOcorrencias0Dadox += 1
        # if (qtdOcorrencias1Dadox == 0):
        #    qtdOcorrencias1Dadox += 1

        resultado0 *= qtdOcorrencias0Dadox / len(listaAlvo0)
        resultado1 *= qtdOcorrencias1Dadox / len(listaAlvo1)

    # probabilidade de ocorrer dado 
    resultado0 *= len(listaAlvo0) / len(listaCompleta)
    resultado1 *= len(listaAlvo1) / len(listaCompleta)

    maiorResultado = list()

    if (resultado0 > resultado1):
        maiorResultado.append(0)
        maiorResultado.append(resultado0)
    else:
        maiorResultado.append(1)
        maiorResultado.append(resultado1)

    return maiorResultado


listaCompleta = lista[1:]

tam = 1000
treinamento = 100

qtdAcertos = 0
qtdErros = 0

list_taxa_media_erros = []

while treinamento < tam:

    qtdAcertoMax = 0
    qtdAcertoMin = 901
    # Quantidade acertos totais das 30 rodadas para retirar a media.
    qtdTotalAcertos = 0

    # Listas com as quantidades de acertos por classe
    qtdAcertosPorGenero = [0, 0]
    qtdAcertosPorIdade = [0, 0, 0, 0, 0]
    qtdAcertosPorEscolaridade = [0, 0, 0, 0]
    qtdAcertosPorProfissao = [0, 0, 0, 0, 0]
    listaQtdAcertos = [qtdAcertosPorGenero, qtdAcertosPorIdade, qtdAcertosPorEscolaridade, qtdAcertosPorProfissao]

    # Listas com as quantidades de testes por classe
    qtdTestesPorGenero = [0, 0]
    qtdTestesPorIdade = [0, 0, 0, 0, 0]
    qtdTestesPorEscolaridade = [0, 0, 0, 0]
    qtdTestesPorProfissao = [0, 0, 0, 0, 0]
    listaQtdTestes = [qtdTestesPorGenero, qtdTestesPorIdade, qtdTestesPorEscolaridade, qtdTestesPorProfissao]

    print('Execucao com tamanho de treinamento: ' + str(treinamento))
    # Executa 30 vezes para obter taxa de acerto medias max e min + medias por classe
    for repeticao in range(30):

        # Faz o embaralhamento da base completa e separa as bases para treinamento e teste
        random.shuffle(listaCompleta)
        listaTreinamento = listaCompleta[:treinamento]
        listaTeste = listaCompleta[treinamento:]

        # Quantidade de acerto por rodada
        qtdAcertos = 0

        for x in listaTeste:
            resultado = calcularProbabilidadeBayes(x, listaTreinamento)

            # Conta a quantidade de testes para cada classe
            for index in range(len(listaQtdTestes)):
                listaQtdTestes[index][x[index]] += 1

            # Acerto
            if x[4] == resultado[0]:
                qtdAcertos += 1
                qtdTotalAcertos += 1

                # Separa os acertos por classe
                for index in range(len(listaQtdAcertos)):
                    listaQtdAcertos[index][x[index]] += 1

        if (qtdAcertoMax < qtdAcertos):
            qtdAcertoMax = qtdAcertos
        if (qtdAcertoMin > qtdAcertos):
            qtdAcertoMin = qtdAcertos

    print('Taxa média acertos: ' + str((qtdTotalAcertos / 30) / (1000 - treinamento)))

    taxa_m_acertos = (qtdTotalAcertos / 30) / (1000 - treinamento)
    taxa_m_erros = 1 - taxa_m_acertos

    print('Taxa média erros: ' + str(taxa_m_erros))

    list_taxa_media_erros.append(taxa_m_erros*100)

    print('Taxa de acerto max: ' + str(qtdAcertoMax / (1000 - treinamento)))
    print('Taxa de acerto min: ' + str(qtdAcertoMin / (1000 - treinamento)))

    print('Taxa media de acertos por Genero:')
    for i in range(len(qtdAcertosPorGenero)):
        print(str(i) + ' - ' + str((qtdAcertosPorGenero[i]) / qtdTestesPorGenero[i]))
    print('Taxa media de acertos por Idade:')
    for i in range(len(qtdAcertosPorIdade)):
        print(str(i) + ' - ' + str((qtdAcertosPorIdade[i]) / qtdTestesPorIdade[i]))
    print('Taxa media de acertos por Escolaridade:')
    for i in range(len(qtdAcertosPorEscolaridade)):
        print(str(i) + ' - ' + str((qtdAcertosPorEscolaridade[i]) / qtdTestesPorEscolaridade[i]))
    print('Taxa media de acertos por Profissao:')
    for i in range(len(qtdAcertosPorProfissao)):
        print(str(i) + ' - ' + str((qtdAcertosPorProfissao[i]) / qtdTestesPorProfissao[i]))

    treinamento += 100

generate_graphic_pdf(list_taxa_media_erros)